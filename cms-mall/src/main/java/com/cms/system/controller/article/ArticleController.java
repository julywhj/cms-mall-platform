package com.cms.system.controller.article;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cms.system.common.result.PageResult;
import com.cms.system.common.result.Result;
import com.cms.system.model.form.ArticleFrom;
import com.cms.system.model.query.ArticlePageQuery;
import com.cms.system.model.vo.ArticleVO;
import com.cms.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.cms.system.service.ArticleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;

/**
 * @author wanghongjie
 */
@Tag(name = "01.文章管理")
@RestController
@RequestMapping("/api/v1/article")
@RequiredArgsConstructor
@Slf4j
public class ArticleController {
    private final ArticleService articleService;

    /**
     * @param articleFrom 标签请求对象
     * @return 新增结果
     */
    @Operation(summary = "新增文章")
    @PostMapping
    @PreventDuplicateSubmit
    public Result<ArticleFrom> createArticle(@RequestBody ArticleFrom articleFrom) {
        ArticleFrom result = articleService.createArticle(articleFrom);
        return Result.success(result);
    }






}
