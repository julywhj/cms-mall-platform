package com.cms.system.controller.article;

import com.cms.system.common.result.Result;
import com.cms.system.model.form.TagsFrom;
import com.cms.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.cms.system.service.TagsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wanghongjie
 */
@Tag(name = "标签管理")
@RestController
@RequestMapping("/api/v2/tags")
@RequiredArgsConstructor
@Slf4j
public class TagsController {
    private final TagsService tagsService;

    /**
     * @param tags 标签请求对象
     * @return 新增结果
     */
    @Operation(summary = "新增标签")
    @PostMapping
    @PreventDuplicateSubmit
    public Result<TagsFrom> saveTags(@RequestBody TagsFrom tags) {
        TagsFrom result = tagsService.saveTags(tags);
        return Result.success(result);
    }


    @Operation(summary = "查询标签")
    @GetMapping("/selectTagsByName")
    public Result<List<TagsFrom>> selectTagsByName(@RequestParam("name") String name) {
        List<TagsFrom> result = tagsService.selectTagsByName(name);
        return Result.success(result);
    }


}
