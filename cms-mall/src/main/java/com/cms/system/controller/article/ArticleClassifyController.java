package com.cms.system.controller.article;

import com.cms.system.common.model.Option;
import com.cms.system.common.result.Result;
import com.cms.system.model.form.TagsFrom;
import com.cms.system.model.vo.ArticleClassifyVO;
import com.cms.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.cms.system.service.ArticleClassifyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wanghongjie
 */
@Tag(name = "01.文章分类")
@RestController
@RequestMapping("/api/v2/articleClassify")
@RequiredArgsConstructor
@Slf4j
public class ArticleClassifyController {
    private final ArticleClassifyService articleClassifyService;

    @Operation(summary = "文章分类展示树")
    @GetMapping("/selectArticleClassifyTree")
    public Result<List<Option<Long>>> selectArticleClassifyTree() {
        List<Option<Long>> options = articleClassifyService.selectArticleClassify();
        return Result.success(options);
    }
}
