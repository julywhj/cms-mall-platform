package com.cms.system.controller.api;

import com.cms.system.common.result.Result;
import com.cms.system.model.form.UserForm;
import com.cms.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.cms.system.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 平台注册中心:手机号注册、微信注册
 *
 * @author wanghongjie
 */
@Tag(name = "01.平台注册中心:手机号注册、微信注册")
@RestController
@RequestMapping("/api/v2/register")
@RequiredArgsConstructor
@Slf4j
public class UserRegisterController {

    private final SysUserService userService;

    @Operation(summary = "手机号验证码注册")
    @PostMapping("/phoneRegister")
    @PreventDuplicateSubmit
    public Result<Boolean> phoneRegister(
            @RequestBody @Valid UserForm userForm
    ) {
        boolean result = userService.phoneRegister(userForm);
        return Result.judge(result);
    }

}
