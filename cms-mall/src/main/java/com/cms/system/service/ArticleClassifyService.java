package com.cms.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cms.system.common.model.Option;
import com.cms.system.model.entity.ArticleClassify;

import java.util.List;

/**
 * @author wanghongjie
 */
public interface ArticleClassifyService extends IService<ArticleClassify> {
    /**
     * 查询分类
     *
     * @return 联级分类
     */
    List<Option<Long>> selectArticleClassify();

}
