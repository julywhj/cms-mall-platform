package com.cms.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cms.system.model.entity.Tags;
import com.cms.system.model.form.TagsFrom;

import java.util.List;

/**
 * @author wanghongjie
 */
public interface TagsService extends IService<Tags> {
    TagsFrom saveTags(TagsFrom tags);

    /**
     * 根据名称模糊查询标签
     *
     * @param name 名称
     * @return
     */
    List<TagsFrom> selectTagsByName(String name);
}
