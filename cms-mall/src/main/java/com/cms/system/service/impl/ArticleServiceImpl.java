package com.cms.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cms.system.common.constant.ArticleConstants;
import com.cms.system.converter.ArticleConverter;
import com.cms.system.mapper.ArticleMapper;
import com.cms.system.model.entity.Article;
import com.cms.system.model.form.ArticleFrom;
import com.cms.system.model.query.ArticlePageQuery;
import com.cms.system.model.vo.ArticleVO;
import com.cms.system.security.util.SecurityUtils;
import com.cms.system.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author wanghongjie
 */
@Service
@RequiredArgsConstructor
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {
    private final ArticleConverter articleConverter;
    private final StringRedisTemplate stringRedisTemplate;

    @Override
    public ArticleFrom createArticle(ArticleFrom articleFrom) {
        Article article = articleConverter.fromToEntity(articleFrom);
        if (Objects.nonNull(SecurityUtils.getUser())) {
            article.setAuthor(SecurityUtils.getUser().getUsername());
        }
        String classifyId = articleFrom.getClassifyIds();
        String[] split = classifyId.split(",");
        String c = split[split.length - 1];
        article.setClassifyId(Long.valueOf(c));
        article.setStatus(1);
        // 新增
        this.baseMapper.insert(article);
        return articleConverter.entityToFrom(article);
    }


    @Override
    public Page<ArticleVO> listArticle(ArticlePageQuery articlePageQuery) {
        // 查询参数
        int pageNum = articlePageQuery.getPageNum();
        int pageSize = articlePageQuery.getPageSize();
        String keywords = articlePageQuery.getKeywords();
        // 查询数据
        Page<Article> articlePage = this.page(
                new Page<>(pageNum, pageSize),
                new LambdaQueryWrapper<Article>()
                        .and(StrUtil.isNotBlank(keywords),
                                wrapper ->
                                        wrapper.like(StrUtil.isNotBlank(keywords), Article::getArticleContent, keywords)
                                                .or()
                                                .like(StrUtil.isNotBlank(keywords), Article::getArticleIntroductory, keywords)
                                                .or()
                                                .like(StrUtil.isNotBlank(keywords), Article::getTitle, keywords)
                        ).orderByDesc(Article::getCreateTime)
        );
        return articleConverter.entity2Page(articlePage);
    }


    @Override
    public ArticleVO getArticleInfo(Long id) {
        Article article = this.baseMapper.selectById(id);
        ArticleVO articleVO = articleConverter.entityToVo(article);
        loadEventsNum(articleVO);
        return articleVO;
    }

    /**
     * 加载文章事件数量
     *
     * @param articleVO 文章VO
     */
    private void loadEventsNum(ArticleVO articleVO) {
        String readNum = stringRedisTemplate.opsForValue().get(ArticleConstants.READ_NUM_PREFIX.concat(articleVO.getId().toString()));
        String commentNum = stringRedisTemplate.opsForValue().get(ArticleConstants.COMMENT_NUM_PREFIX.concat(articleVO.getId().toString()));
        String likeNum = stringRedisTemplate.opsForValue().get(ArticleConstants.LIKE_NUM_PREFIX.concat(articleVO.getId().toString()));
        // 加载文章阅读数量
        articleVO.setReadNum(StringUtils.isEmpty(readNum) ? 0 : Integer.parseInt(readNum));
        articleVO.setCommentNum(StringUtils.isEmpty(commentNum) ? 0 : Integer.parseInt(commentNum));
        articleVO.setLikeNum(StringUtils.isEmpty(likeNum) ? 0 : Integer.parseInt(likeNum));
    }
}
