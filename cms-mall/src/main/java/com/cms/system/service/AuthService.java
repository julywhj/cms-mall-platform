package com.cms.system.service;

import com.cms.system.model.dto.CaptchaResult;
import com.cms.system.model.dto.LoginResult;
import com.cms.system.model.dto.SmsCodeResult;

/**
 * 认证服务接口
 *
 * @author wanghongjie
 * @since 2.4.0
 */
public interface AuthService {

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @return 登录结果
     */
    LoginResult login(String username, String password);

    /**
     * 登出
     */
    void logout();

    /**
     * 获取验证码
     *
     * @return 验证码
     */
    CaptchaResult getCaptcha();

    /**
     * 发送短信验证码
     *
     * @param phone 手机号
     */
    SmsCodeResult sendSms(String phone);

}
