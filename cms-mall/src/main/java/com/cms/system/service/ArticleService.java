package com.cms.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cms.system.model.entity.Article;
import com.cms.system.model.form.ArticleFrom;
import com.cms.system.model.query.ArticlePageQuery;
import com.cms.system.model.vo.ArticleVO;

/**
 * @author wanghongjie
 */
public interface ArticleService extends IService<Article> {
    /**
     * 创建文章
     *
     * @param articleFrom 文章请求对象
     * @return
     */
    ArticleFrom createArticle(ArticleFrom articleFrom);

    /**
     * 文章列表查询
     *
     * @param articleFrom 文章列表
     * @return
     */
    Page<ArticleVO> listArticle(ArticlePageQuery articleFrom);

    /**
     * 获取文章详情
     *
     * @param id 文章id
     * @return
     */
    ArticleVO getArticleInfo(Long id);
}
