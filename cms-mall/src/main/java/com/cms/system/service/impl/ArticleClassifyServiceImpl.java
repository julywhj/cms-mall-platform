package com.cms.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cms.system.common.model.Option;
import com.cms.system.mapper.ArticleClassifyMapper;
import com.cms.system.model.entity.ArticleClassify;
import com.cms.system.service.ArticleClassifyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author wanghongjie
 */
@Service
@RequiredArgsConstructor
public class ArticleClassifyServiceImpl extends ServiceImpl<ArticleClassifyMapper, ArticleClassify> implements ArticleClassifyService {

    @Override
    public List<Option<Long>> selectArticleClassify() {
        List<ArticleClassify> articleClassifies = this.list();

        Set<Long> articleClassifyIds = articleClassifies.stream()
                .map(ArticleClassify::getId)
                .collect(Collectors.toSet());

        Set<Long> parentIds = articleClassifies.stream()
                .map(ArticleClassify::getParentId)
                .collect(Collectors.toSet());
        List<Long> rootIds = CollectionUtil.subtractToList(parentIds, articleClassifyIds);

        // 递归生成分类树形列表
        return rootIds.stream()
                .flatMap(rootId -> recurDeptTreeOptions(rootId, articleClassifies).stream())
                .toList();
    }

    /**
     * 递归生成分类表格层级列表
     *
     * @param parentId 父ID
     * @param articleClassifies 分类列表
     * @return 分类列表表格层级列表
     */
    public static List<Option<Long>> recurDeptTreeOptions(long parentId, List<ArticleClassify> articleClassifies) {
        List<Option<Long>> list = CollectionUtil.emptyIfNull(articleClassifies).stream()
                .filter(articleClassify -> articleClassify.getParentId().equals(parentId))
                .map(articleClassify -> {
                    Option<Long> option = new Option<>(articleClassify.getId(), articleClassify.getClassifyName());
                    List<Option<Long>> children = recurDeptTreeOptions(articleClassify.getId(), articleClassifies);
                    if (CollectionUtil.isNotEmpty(children)) {
                        option.setChildren(children);
                    }
                    return option;
                })
                .collect(Collectors.toList());
        return list;
    }
}
