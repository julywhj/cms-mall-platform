package com.cms.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cms.system.common.enums.StatusEnum;
import com.cms.system.converter.TagsConverter;
import com.cms.system.mapper.TagsMapper;
import com.cms.system.model.entity.Tags;
import com.cms.system.model.form.TagsFrom;
import com.cms.system.security.util.SecurityUtils;
import com.cms.system.service.TagsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author wanghongjie
 */
@Service
@RequiredArgsConstructor
public class TagsServiceImpl extends ServiceImpl<TagsMapper, Tags> implements TagsService {
    private final TagsConverter tagsConverter;

    @Override
    public TagsFrom saveTags(TagsFrom tagsFrom) {
        checkTags(tagsFrom);
        Tags tags = tagsConverter.form2Entity(tagsFrom);
        LambdaQueryWrapper<Tags> wrapper = new LambdaQueryWrapper<>();
        Long l = this.baseMapper.selectCount(wrapper);
        tags.setStatus(StatusEnum.ENABLE.getValue());
        tags.setDefaulted(StatusEnum.ENABLE.getValue());
        tags.setCreateUser(Objects.nonNull(SecurityUtils.getUser()) ? SecurityUtils.getUser().getUsername() : "sys");
        tags.setSort(Math.toIntExact(l + 1));
        this.baseMapper.insert(tags);
        return tagsConverter.entity2Form(tags);
    }

    public void checkTags(TagsFrom tagsFrom) {
        LambdaQueryWrapper<Tags> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Tags::getTagName, tagsFrom.getTagName());
        if (this.baseMapper.selectCount(wrapper) > 0) {
            throw new RuntimeException("标签已存在");
        }
    }

    @Override
    public List<TagsFrom> selectTagsByName(String name) {
        LambdaQueryWrapper<Tags> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(Tags::getTagName, name);
        List<Tags> tags = this.baseMapper.selectList(wrapper);
        return tagsConverter.entityList2FormList(tags);
    }
}
