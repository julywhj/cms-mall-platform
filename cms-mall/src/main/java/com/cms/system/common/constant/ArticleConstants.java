package com.cms.system.common.constant;

/**
 * 文章相关缓存前缀
 *
 * @author wanghongjie
 */
public class ArticleConstants {

    public static final String ARTICLE_PREFIX = "article:";
    /**
     * 文章详情
     */
    public static final String ARTICLE_INFO_PREFIX = ARTICLE_PREFIX.concat("info:");
    /**
     * 阅读数量
     */
    public static final String READ_NUM_PREFIX = ARTICLE_PREFIX.concat("readNum:");
    /**
     * 评论数量
     */
    public static final String COMMENT_NUM_PREFIX = ARTICLE_PREFIX.concat("commentNum:");
    /**
     * 点赞
     */
    public static final String LIKE_NUM_PREFIX = ARTICLE_PREFIX.concat("likeNum:");
    /**
     * 咖啡豆数量
     */
    public static final String COFFEE_BEAN_NUMBER_PREFIX = ARTICLE_PREFIX.concat("coffeeBeanNumber:");
}
