package com.cms.system.common.constant;

/**
 * 缓存常量
 *
 * @author wanghongjie
 * @since 2023/11/24
 */
public interface SecurityConstants {

    /**
     * 验证码缓存前缀
     */
    String CAPTCHA_CODE_PREFIX = "captcha_code:";

    /**
     * 角色和权限缓存前缀
     */
    String ROLE_PERMS_PREFIX = "role_perms:";

    /**
     * 黑名单Token缓存前缀
     */
    String BLACKLIST_TOKEN_PREFIX = "token:blacklist:";
    /**
     * 短信验证码
     */
    String SMS_CODE_PREFIX = "sms_code:";


}
