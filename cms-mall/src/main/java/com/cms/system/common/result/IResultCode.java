package com.cms.system.common.result;

/**
 * @author wanghongjie
 **/
public interface IResultCode {

    String getCode();

    String getMsg();

}
