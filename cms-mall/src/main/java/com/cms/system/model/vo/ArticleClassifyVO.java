package com.cms.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author wanghongjie
 */
@Schema(description = "文章分类视图")
@Data
public class ArticleClassifyVO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 分类名称
     */
    private String classifyName;
    /**
     * 父级分类
     */
    private Long parentId;

    /**
     * 创建者
     */
    private String createUser;
    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer status;

    /**
     * 是否默认(1:是;0:否)
     */
    private Integer defaulted;
    /**
     * 子分类
     */
    private List<ArticleClassifyVO> children;
}
