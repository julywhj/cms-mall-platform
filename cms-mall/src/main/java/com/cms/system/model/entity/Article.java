package com.cms.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cms.system.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文章
 *
 * @author wanghongjie
 */
@TableName(value = "article")
@EqualsAndHashCode(callSuper = true)
@Data
public class Article extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 文章简介
     */
    private String articleIntroductory;
    /**
     * 文章内容
     */
    private String articleContent;
    /**
     * 文章分类
     */
    private Long classifyId;
    /**
     * 文章标签
     */
    private String articleTags;
    /**
     * 文章封面
     */
    private String coverImg;
    /**
     * 作者
     */
    private String author;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer status;

    /**
     * 是否默认(1:是;0:否)
     */
    private Integer defaulted;

    /**
     * 备注
     */
    private String remark;

    /**
     * 逻辑删除标识(0:未删除;1:已删除)
     */
    private Integer deleted;

}
