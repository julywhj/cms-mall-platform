package com.cms.system.model.form;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author wanghongjie
 */
@Data
public class TagsFrom {
    @Schema(description = "标签ID")
    private Long id;

    /**
     * 文章标签
     */
    @Schema(description = "标签名称")
    private String tagName;

    /**
     * 创建者
     */
    @Schema(description = "创建者")
    private String createUser;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;
}
