package com.cms.system.model.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * @author wanghongjie
 */
@Data
public class ArticleFrom {

    /**
     * 主键
     */
    private Long id;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 文章简介
     */
    private String articleIntroductory;
    /**
     * 文章内容
     */
    private String articleContent;
    /**
     * 文章分类
     */
    private String classifyIds;
    /**
     * 文章标签
     */
    private String articleTags;
    /**
     * 文章封面
     */
    private String coverImg;
    /**
     * 作者
     */
    private String author;
    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer status;

    /**
     * 是否默认(1:是;0:否)
     */
    private Integer defaulted;
    /**
     * 备注
     */
    private String remark;

    @Schema(description="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

}
