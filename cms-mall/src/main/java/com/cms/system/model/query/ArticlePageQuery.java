package com.cms.system.model.query;

import com.cms.system.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 文章分页查询对象
 *
 * @author wanghongjie
 */
@Data
public class ArticlePageQuery extends BasePageQuery {
    @Schema(description = "关键字(文章标题/内容/简介/作者)")
    private String keywords;
}
