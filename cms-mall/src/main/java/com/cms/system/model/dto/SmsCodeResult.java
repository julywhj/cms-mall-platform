package com.cms.system.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

/**
 * @author wanghongjie
 */
@Schema(description = "验证码响应对象")
@Data
@Builder
public class SmsCodeResult {
    @Schema(description = "uid")
    private String uid;
}
