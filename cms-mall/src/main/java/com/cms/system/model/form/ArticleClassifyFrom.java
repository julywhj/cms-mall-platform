package com.cms.system.model.form;

import lombok.Data;

/**
 * @author wanghongjie
 */
@Data
public class ArticleClassifyFrom {
    /**
     * 主键
     */
    private Long id;
    /**
     * 分类名称
     */
    private String classifyName;
    /**
     * 父级分类
     */
    private Long parentId;

    /**
     * 创建者
     */
    private String createUser;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer status;

    /**
     * 是否默认(1:是;0:否)
     */
    private Integer defaulted;
}
