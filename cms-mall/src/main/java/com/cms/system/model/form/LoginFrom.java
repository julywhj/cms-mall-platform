package com.cms.system.model.form;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author wanghongjie
 */
@Schema(description = "登陆请求对象")
@Data
public class LoginFrom {
    @Schema(description = "用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "验证码-短信登陆不可以为空")
    private String code;

    @Schema(description = "uid-短信登陆不可以为空")
    private String uid;

}
