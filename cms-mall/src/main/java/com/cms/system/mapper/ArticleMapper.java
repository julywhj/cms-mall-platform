package com.cms.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cms.system.model.entity.Article;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wanghongjie
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

}
