package com.cms.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cms.system.model.entity.ArticleClassify;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文章分类
 *
 * @author wanghongjie
 */
@Mapper
public interface ArticleClassifyMapper extends BaseMapper<ArticleClassify> {
}
