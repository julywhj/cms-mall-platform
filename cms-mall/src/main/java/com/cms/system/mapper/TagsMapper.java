package com.cms.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cms.system.model.entity.Tags;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wanghongjie
 */
@Mapper
public interface TagsMapper extends BaseMapper<Tags> {
}