package com.cms.system.security.service;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * @author wanghongjie
 */
public class CaptureCodeAuthenticationToken extends UsernamePasswordAuthenticationToken {
    public CaptureCodeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }
}
