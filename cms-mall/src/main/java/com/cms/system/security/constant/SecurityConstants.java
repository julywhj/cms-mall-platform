package com.cms.system.security.constant;

/**
 * Security 常量
 *
 * @author wanghongjie
 * @since 2.0.0
 */
public interface SecurityConstants {

    /**
     * 登录接口路径
     */
    String LOGIN_PATH = "/api/v1/auth/login";

    String LOGIN_PATH_V2 = "/api/v1/auth/loginV2";

    String SMS_LOGIN_PATH = "/api/v1/auth/smsCodeLogin";

}
