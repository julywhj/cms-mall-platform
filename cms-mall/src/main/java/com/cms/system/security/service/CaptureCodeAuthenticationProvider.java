package com.cms.system.security.service;

import cn.hutool.json.JSONUtil;
import com.alibaba.druid.util.StringUtils;
import com.cms.system.common.constant.SecurityConstants;
import com.cms.system.model.dto.UserAuthInfo;
import com.cms.system.model.form.LoginFrom;
import com.cms.system.security.model.SysUserDetails;
import com.cms.system.service.SysUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author wanghongjie
 */
@Slf4j
@RequiredArgsConstructor
public class CaptureCodeAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private final RedisTemplate<String, Object> redisTemplate;
    private final SysUserService sysUserService;

    @Override
    public boolean supports(Class<?> authentication) {
        return (CaptureCodeAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (authentication.getPrincipal() == null) {
            throw new BadCredentialsException("Bad credentials " + authentication.getPrincipal().toString());
        }
        if (authentication.getCredentials() == null) {
            throw new BadCredentialsException("Bad credentials " + authentication.getPrincipal().toString());
        }
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        CaptureCodeAuthenticationToken token = (CaptureCodeAuthenticationToken) authentication;
        Object principal = token.getPrincipal();
        LoginFrom loginFrom = JSONUtil.toBean(JSONUtil.toJsonStr(principal), LoginFrom.class);
        log.info("loginFrom:{},{},{}", loginFrom.getCode(), loginFrom.getUid(), loginFrom.getUsername());
        String cacheVerifyCode = (String) redisTemplate.opsForValue().get(SecurityConstants.SMS_CODE_PREFIX.concat(loginFrom.getUid()));
        if (StringUtils.isEmpty(cacheVerifyCode) || !cacheVerifyCode.equals(loginFrom.getCode())) {
            throw new UsernameNotFoundException("username not fund!");
        }
        redisTemplate.delete(SecurityConstants.SMS_CODE_PREFIX.concat(loginFrom.getUid()));
        UserAuthInfo userAuthInfo = sysUserService.getUserAuthInfo(loginFrom.getUsername());
        return new SysUserDetails(userAuthInfo);
    }


}
