package com.cms.system.security.service;

import com.alibaba.druid.util.StringUtils;
import com.cms.system.model.dto.UserAuthInfo;
import com.cms.system.model.entity.SysUser;
import com.cms.system.security.model.SysUserDetails;
import com.cms.system.service.SysUserService;
import groovy.util.logging.Slf4j;
import jodd.util.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author wanghongjie
 */
@Slf4j
@Service("smsUserDetailsService")
@RequiredArgsConstructor
public class SmsUserDetailsService implements UserDetailsService {
    private final SysUserService sysUserService;

    /**
     * loadUserByUsername
     *
     * @param phone 手机号
     * @return LoginUser
     */
    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        UserAuthInfo user = sysUserService.getUserAuthInfo(phone);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException(phone);
        }
        return new SysUserDetails(user);
    }


}
