package com.cms.system.converter;

import com.cms.system.model.entity.ArticleClassify;
import com.cms.system.model.form.ArticleClassifyFrom;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 分类转换器
 *
 * @author wanghongjie
 */
@Mapper(componentModel = "spring")
public interface ArticleClassifyConverter {
    ArticleClassify fromToEntity(ArticleClassifyFrom articleClassifyFrom);

    ArticleClassifyFrom entityToFrom(ArticleClassify articleClassify);

    List<ArticleClassifyFrom> entityList2FormList(List<ArticleClassify> articleClassifies);
}
