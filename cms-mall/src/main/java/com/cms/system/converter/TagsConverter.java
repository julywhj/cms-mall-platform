package com.cms.system.converter;

import com.cms.system.model.entity.Tags;
import com.cms.system.model.form.TagsFrom;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author wanghongjie
 */
@Mapper(componentModel = "spring")
public interface TagsConverter {
   public Tags form2Entity(TagsFrom tagsFrom);

   public TagsFrom entity2Form(Tags entity);

   List<TagsFrom> entityList2FormList(List<Tags> tags);
}
