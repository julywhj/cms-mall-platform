package com.cms.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cms.system.model.entity.Article;
import com.cms.system.model.form.ArticleFrom;
import com.cms.system.model.vo.ArticleVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author wanghongjie
 */
@Mapper(componentModel = "spring")
public interface ArticleConverter {
    Article fromToEntity(ArticleFrom articleFrom);

    ArticleFrom entityToFrom(Article article);

    List<ArticleFrom> entityList2FormList(List<Article> articles);

    ArticleVO entityToVo(Article article);

    Page<ArticleVO> entity2Page(Page<Article> page);
}
