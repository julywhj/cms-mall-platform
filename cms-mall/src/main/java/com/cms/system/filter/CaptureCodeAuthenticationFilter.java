package com.cms.system.filter;

import com.cms.system.common.result.Result;
import com.cms.system.common.result.ResultCode;
import com.cms.system.common.util.ResponseUtils;
import com.cms.system.model.dto.LoginResult;
import com.cms.system.security.service.CaptureCodeAuthenticationProvider;
import com.cms.system.security.service.CaptureCodeAuthenticationToken;
import com.cms.system.security.util.JwtUtils;
import com.cms.system.service.SysUserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * @author wanghongjie
 */
public class CaptureCodeAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String DEFAULT_LOGIN_URL = "/api/v1/auth/smsCodeLogin";

    public CaptureCodeAuthenticationFilter(RedisTemplate<String, Object> redisTemplate, SysUserService sysUserService) {
        super(DEFAULT_LOGIN_URL, new ProviderManager(new CaptureCodeAuthenticationProvider(redisTemplate, sysUserService)));
        super.setAuthenticationSuccessHandler((request, response, authentication) -> {
            // 登录成功
            String accessToken = JwtUtils.generateToken(authentication);
            ResponseUtils.writeSuccessMsg(response, Result.success(LoginResult.builder().tokenType("Bearer").accessToken(accessToken).build()));
        });
        super.setAuthenticationFailureHandler((request, response, exception) -> {
            // 用户名或密码错误
            ResponseUtils.writeErrMsg(response, ResultCode.VERIFY_SMS_CODE_ERROR);
        });
    }

    public CaptureCodeAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(DEFAULT_LOGIN_URL, authenticationManager);
    }

    public CaptureCodeAuthenticationFilter(String defaultFilterProcessesUrl, AuthenticationManager authenticationManager) {
        super(defaultFilterProcessesUrl, authenticationManager);
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException {
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
        HttpServletRequest requestWrapper = new BodyReaderHttpServletRequestWrapper(request);
        return this.getAuthenticationManager().authenticate(buildCaptureCodeAuthenticationToken(requestWrapper));
    }

    protected CaptureCodeAuthenticationToken buildCaptureCodeAuthenticationToken(HttpServletRequest request) {
        return new CaptureCodeAuthenticationToken(getRequestPayload(request), "");
    }

    private String getRequestPayload(HttpServletRequest req) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = req.getReader();) {
            char[] buff = new char[1024];
            int len;
            while ((len = reader.read(buff)) != -1) {
                sb.append(buff, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
