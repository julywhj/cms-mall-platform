/* eslint-disable no-param-reassign */
// craco.config.js
const path = require('path');
const CracoLessPlugin = require('craco-less');
const { loaderByName } = require('@craco/craco');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const WebpackBar = require('webpackbar');
const PRODUCTION = 'production';

const webpackPlugins = [new WebpackBar()];
if (process.env.REACT_APP_ANY === 'true') {
    webpackPlugins.push(new BundleAnalyzerPlugin());
}

module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        javascriptEnabled: true,
                    },
                    additionalData: async (content, loaderContext) => {
                        return content;
                    },
                },
                modifyLessRule(lessRule, context) {
                    lessRule.exclude = /\.m\.less$/;
                    return lessRule;
                },
                modifyLessModuleRule(lessModuleRule, context) {
                    // Configure the file suffix
                    lessModuleRule.test = /\.m\.less$/;

                    // Configure the generated local ident name.
                    const cssLoader = lessModuleRule.use.find(loaderByName('css-loader'));
                    cssLoader.options.modules = {
                        localIdentName: context.env == PRODUCTION ? '[hash:base64]' : '[path][name]__[local]',
                    };

                    return lessModuleRule;
                },
            },
        },
    ],
    devServer: {
        allowedHosts: ['localhost:3000', '127.0.0.1:3000'],
        headers: {
            'Access-Control-Allow-Origin': '*',
            Origin: 'http://localhost:3000',
        },
    },
    webpack: {
        configure: (webpackConfig, { env, paths }) => {
            if (env === PRODUCTION) {
                webpackConfig.devtool = false;
                const TerserPlugin = webpackConfig.optimization.minimizer.find((i) => i.constructor.name === 'TerserPlugin');
                if (TerserPlugin) {
                    TerserPlugin.options.terserOptions.compress['drop_console'] = true;
                    // TerserPlugin.options.terserOptions.compress['remove']
                }
            }
            return webpackConfig;
        },
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '@declaration': path.resolve(__dirname, './src/declaration'),
            '@utils': path.resolve(__dirname, './src/utils'),
            '@apis': path.resolve(__dirname, './src/services/apis'),
            '@config': path.resolve(__dirname, './src/config'),
            '@components': path.resolve(__dirname, './src/components'),
        },
        plugins: webpackPlugins,
    },
};
