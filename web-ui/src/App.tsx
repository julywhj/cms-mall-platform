import React from 'react';
import { Layout } from 'antd';
import { Content, Header } from 'antd/es/layout/layout';
import '@/styles/common.less';
import LoginHeader from './pages/header/Header';
import LoginContent from './pages/login/LoginContent';
import Home from './pages/home/Home';
import { Routes, Route } from 'react-router-dom';
import ArticlePage from './pages/article/list/ArticlePage';
import EditArticle from './pages/article/edit/EditArticle';
import ArticleInfo from './pages/article/info/ArticleInfo';

const App: React.FC = () => (
    <Layout className="layout-style">
        <Header className="header-style">
            <LoginHeader />
        </Header>
        <Content className="content-style">
            <Routes>
                <Route index element={<Home />} />
                <Route path="/article" element={<ArticlePage />} />
                <Route path="/articleEdit" element={<EditArticle />} />
                <Route path="/login" element={<LoginContent />} />
                <Route path="/articleInfo/:id" element={<ArticleInfo />} />
                <Route path="*" element={<Home />} />
            </Routes>
        </Content>
    </Layout>
);

export default App;
