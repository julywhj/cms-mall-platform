declare interface LoginProps {
    setLoginType: (loginType: string) => void;
}

declare interface PageData {
    total: number;
    list: any;
}
