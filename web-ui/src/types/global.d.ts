declare interface LoginResult {
    accessToken: string;
}

declare interface TagsType {
    id?: string;
    tagName: string;
}

declare interface Option {
    value: string | number;
    label: string;
    children?: Option[];
}

declare interface Article {
    /**
     * 主键
     */
    id?: number;
    /**
     * 文章标题
     */
    title: string;
    /**
     * 文章简介
     */
    articleIntroductory: string;
    /**
     * 文章内容
     */
    articleContent: string;
    /**
     * 文章分类
     */
    classifyIds: string;
    /**
     * 文章标签
     */
    articleTags: string;
    /**
     * 文章封面
     */
    coverImg: string | undefined;
    /**
     * 作者
     */
    author?: string;

    /**
     * 状态(1:正常;0:禁用)
     */
    status?: number;

    /**
     * 备注
     */
    remark?: string;
    /**
     * 阅读数量
     */
    readNum?: number;
    /**
     * 评价数
     */
    commentNum?: number;
    /**
     * 点赞数
     */
    likeNum?: number;
    /**
     * 咖啡豆
     */
    coffeeBeanNumber?: number;
    //创建时间
    createTime?: string;
}
//评论
declare interface CommentType {
    //用户ID
    userId: number;
    //评论内容
    commentContent: string;
    //文章ID
    articleId: number;
    //头像
    avatar: string;
    //用户名称
    userName: string;
    //评论时间
    createTime: string;
}
