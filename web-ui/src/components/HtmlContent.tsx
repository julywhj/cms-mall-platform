const HtmlContent = ({ html }: { html: string }) => <div dangerouslySetInnerHTML={{ __html: html }} />;

export default HtmlContent;
