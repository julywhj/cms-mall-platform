import styles from '@/styles/components/comment.m.less';
import { Image } from 'antd';
import TextArea from 'antd/es/input/TextArea';
interface CommentProps {
    articleId: number;
    commentList: CommentType[];
}
const Comment = (props: CommentProps) => {
    return (
        <div className={styles['comment']}>
            <div className={styles['title']}>
                <div>评论</div>
                <div className={styles['comm-num']}>10条</div>
            </div>
            <div className={styles['post']}>
                <div className={styles['post-comm']}>
                    <div className={styles['avatar']}>
                        <Image preview={false} className={styles['avatar']} src="https://store.sumubook.com/sumo/images/Gnarf.png"></Image>
                    </div>
                    <div className="ml-1" style={{ width: '100%' }}>
                        <TextArea showCount maxLength={200} rows={4} />
                    </div>
                </div>
                <div className={styles['post-btn']}>发布</div>
            </div>
            <div className={styles['comm-list']}>
                {props.commentList.map((item, index) => {
                    return (
                        <div key={index} className={styles['comm-item']}>
                            <div className="flex">
                                <div className={styles['avatar']}>
                                    <Image preview={false} className={styles['avatar']} src={item.avatar}></Image>
                                </div>
                                <div className="ml-1">
                                    <div className="flex align-end">
                                        <div className={styles['nickname']}>{item.userName}</div>
                                        <div className={styles['time']}>{item.createTime}</div>
                                    </div>
                                    <div className={styles['content']}>{item.commentContent}</div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};
export default Comment;
