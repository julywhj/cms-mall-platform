import HttpUtils from '@/utils/HttpUtils';
import { Input, message } from 'antd';
import { useCallback, useState } from 'react';
import styles from '@/styles/components/verification-code.m.less';

declare interface VerificationCodeProps {
    setCode: (code: string) => void;
    setUid: (uid: string) => void;
    className: string;
    phone: string;
}

const VerificationCode = (props: VerificationCodeProps) => {
    const [verificationCode, setVerificationCode] = useState<string>('');
    const [sendSmsCodeText, setSendSmsCodeText] = useState<string>('发送验证码');
    const [isSendSmsCode, setIsSendSmsCode] = useState<boolean>(false);
    //发送验证码
    const SendSmsCode = useCallback(async () => {
        if (props.phone === '') {
            message.error('手机号不能为空');
            return;
        }
        if (isSendSmsCode) {
            return;
        }
        setIsSendSmsCode(true);
        let timer = 60;
        const siv = setInterval(() => {
            if (timer <= 0) {
                clearInterval(siv);
                setSendSmsCodeText('重新发送');
                setIsSendSmsCode(false);
            } else {
                timer = timer - 1;
                setSendSmsCodeText('' + timer + 's');
            }
        }, 1000);
        const codeRes: any = await HttpUtils.Get('/api/v1/auth/sendSms', {
            phone: props.phone,
        });
        props.setUid(codeRes.uid);
    }, [isSendSmsCode, props.phone]);

    const OnChangeCode = useCallback((e: string) => {
        setVerificationCode(e);
        props.setCode(e);
    }, []);

    return (
        <Input
            className={props.className}
            placeholder="*请输入验证码"
            suffix={
                <div onClick={() => SendSmsCode()} className={styles['suffix-code']}>
                    {sendSmsCodeText}
                </div>
            }
            value={verificationCode}
            onChange={(e) => OnChangeCode(e.target.value)}
        />
    );
};
export default VerificationCode;
