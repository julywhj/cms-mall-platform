import '@/styles/components/editor.css'; // 引入 css

import React, { useState, useEffect } from 'react';
import { Editor, Toolbar } from '@wangeditor/editor-for-react';
import { IDomEditor, IEditorConfig, IToolbarConfig, DomEditor } from '@wangeditor/editor';
import { upLoadFile } from '@/service/file/file';

interface MyEditorProps {
    html: string;
    setHtml: (html: string) => void;
}

const MyEditor = (props: MyEditorProps) => {
    // editor 实例
    const [editor, setEditor] = useState<IDomEditor | null>(null); // TS 语法
    // const [editor, setEditor] = useState(null)                   // JS 语法

    // 模拟 ajax 请求，异步设置 html
    useEffect(() => {
        if (editor) {
            const toolbar = DomEditor.getToolbar(editor);
            if (toolbar) {
                const curToolbarConfig = toolbar.getConfig();
                console.log(curToolbarConfig.toolbarKeys); // 当前菜单排序和分组
            }
        }
    }, [editor]);

    // 工具栏配置
    const toolbarConfig: Partial<IToolbarConfig> = {}; // TS 语法
    // const toolbarConfig = { }                        // JS 语法

    // 编辑器配置
    const editorConfig: Partial<IEditorConfig> = {
        // TS 语法
        // const editorConfig = {                         // JS 语法
        placeholder: '请输入内容...',
        MENU_CONF: {
            uploadImage: {
                // 自定义图片上传
                async customUpload(file: any, insertFn: any) {
                    upLoadFile(file).then((response) => {
                        const url = response.url;
                        insertFn(url);
                    });
                },
            },
        },
    };

    // 及时销毁 editor ，重要！
    useEffect(() => {
        return () => {
            if (editor == null) return;
            editor.destroy();
            setEditor(null);
        };
    }, [editor]);

    return (
        <>
            <div style={{ border: '0', zIndex: 100 }}>
                <Toolbar editor={editor} defaultConfig={toolbarConfig} mode="default" style={{ borderBottom: '1px solid #ccc' }} />
                <Editor defaultConfig={editorConfig} value={props.html} onCreated={setEditor} onChange={(editor) => props.setHtml(editor.getHtml())} mode="default" style={{ minHeight: '300px', paddingBottom: '50px', overflowY: 'hidden' }} />
            </div>
        </>
    );
};

export default MyEditor;
