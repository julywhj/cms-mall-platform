import { handelPromise } from '@/utils';
import HttpUtils from '@/utils/HttpUtils';

export const SaveTags = async (data: TagsType): Promise<TagsType> => {
    return await handelPromise<any>(HttpUtils.Post('/api/v2/tags', data));
};

export const GetTags = async (name: string): Promise<TagsType[]> => {
    return await handelPromise<any>(HttpUtils.Get('/api/v2/tags/selectTagsByName', { name }));
};
