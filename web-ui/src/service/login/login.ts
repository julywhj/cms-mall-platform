import { handelPromise, putLocalStorage } from '@/utils';
import HttpUtils from '@/utils/HttpUtils';

export const PhoneLoginApi = async (username: string, password: string) => {
    const data = await handelPromise<any>(
        HttpUtils.Post('/api/v1/auth/loginV2', {
            username,
            password,
        })
    );
    putLocalStorage('accessToken', data.accessToken);
    HttpUtils.Login({ Authorization: data.accessToken });
};

export const SmsCodeLogin = async (username: string, code: string, uid: string) => {
    const data = await handelPromise<any>(
        HttpUtils.Post('/api/v1/auth/smsCodeLogin', {
            username,
            code,
            uid,
        })
    );
    putLocalStorage('accessToken', data.accessToken);
    HttpUtils.Login({ Authorization: data.accessToken });
};
