import { getLocalStorage, handelPromise } from '@/utils';
import HttpUtils from '@/utils/HttpUtils';

export const saveArticle = async (data: Article): Promise<Article> => {
    return await handelPromise<any>(HttpUtils.Post('/api/v1/article', data, { Authorization: getLocalStorage('accessToken') }));
};

export const listArticle = async (data: any): Promise<PageData> => {
    return await handelPromise<any>(HttpUtils.Get('/api/v2/article', data));
};

export const infoArticle = async (id: number): Promise<Article> => {
    return await handelPromise<any>(HttpUtils.Get('/api/v2/article/getArticleInfo', { id: id }));
};
