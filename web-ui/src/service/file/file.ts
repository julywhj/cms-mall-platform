import { getLocalStorage, handelPromise } from '@/utils';
import HttpUtils from '@/utils/HttpUtils';
/**
 * 文件API类型声明
 */
export interface FileInfo {
    name: string;
    url: string;
}

export const upLoadFile = async (file: File): Promise<FileInfo> => {
    const formData = new FormData();
    formData.append('file', file);
    return await handelPromise<any>(HttpUtils.UploadFile('/api/v1/files', formData, { Authorization: getLocalStorage('accessToken'), ContentType: 'multipart/form-data' }));
};
