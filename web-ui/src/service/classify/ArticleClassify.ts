import { handelPromise } from '@/utils';
import HttpUtils from '@/utils/HttpUtils';

export const articleClassifyTree = async (): Promise<Option[]> => {
    return await handelPromise<any>(HttpUtils.Get('/api/v2/articleClassify/selectArticleClassifyTree'));
};
