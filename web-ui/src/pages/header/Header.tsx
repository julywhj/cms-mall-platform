import styles from '@/styles/components/header/header.m.less';
import HeaderMenu from './HeaderMenu';
const Header = () => {
    return (
        <div className={styles['login']}>
            <div className={styles['logo']}></div>
            <div className={styles['header-conetent']}>
                <HeaderMenu />
            </div>
            <div className={styles['phone']}>+8618888888888</div>
        </div>
    );
};

export default Header;
