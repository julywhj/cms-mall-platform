import { NavLink } from 'react-router-dom';
import styles from '@/styles/components/header/header-menu.m.less';
import { Button } from 'antd';
import Icon from '@ant-design/icons/lib/components/Icon';
import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';

const SearshSvg = () => (
    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M16.0467236,15.0325538 L18.8621881,17.9413704 C19.3536296,18.4492397 19.3536296,19.2500566 18.8621881,19.7579259 L18.9850663,19.6352702 C18.7619204,19.8681238 18.4518835,20 18.1275975,20 C17.8033115,20 17.4932745,19.8681238 17.2701287,19.6352702 L14.3318058,16.6048739 C12.808254,17.6123086 11.0214309,18.1593426 9.18916736,18.1792872 C4.12998583,18.148738 0.0351732047,14.0998396 9.31551403e-05,9.09334086 C-0.0223874763,4.11142934 4.02682268,0.0476610666 9.06095179,0 C12.6491479,0.00946456093 15.9050654,2.08049606 17.4038421,5.30676477 C18.9026188,8.53303348 18.3713122,12.3270032 16.0424339,15.0283479 L16.0467236,15.0325538 Z M9.18918713,16.3659008 C13.247535,16.3641399 16.5362,13.1073246 16.5350142,9.09121602 C16.5338278,5.0751074 13.2432396,1.82019498 9.18489134,1.82078168 C5.12654308,1.82136837 1.83691567,5.07723207 1.83691567,9.09334086 C1.84982689,13.1057891 5.13449316,16.3548613 9.18914759,16.3659008 L9.18918713,16.3659008 Z"
            id="形状"></path>
    </svg>
);

const MessageSvg = () => (
    <svg width="21px" height="21px" viewBox="0 0 21 21" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M11.6666666,18.3333333 C11.9644184,18.3332877 12.2395718,18.4921102 12.3884608,18.7499632 C12.5373498,19.0078161 12.5373498,19.3255172 12.3884608,19.5833701 C12.2395718,19.8412231 11.9644184,20.0000456 11.6666666,20 L6.66666665,20 C6.36891494,20.0000456 6.09376151,19.8412231 5.9448725,19.5833701 C5.79598348,19.3255172 5.79598348,19.0078161 5.9448725,18.7499632 C6.09376151,18.4921102 6.36891494,18.3332877 6.66666665,18.3333333 L11.6666666,18.3333333 L11.6666666,18.3333333 Z M9.16666665,0 C9.62690395,0 10,0.373096044 10,0.833333339 L10,1.6775 C13.8258333,1.8825 16.8633333,4.97083333 16.8633333,8.74999999 L16.8633333,13.6025 C16.8633333,14.1433333 17.1766666,14.6379166 17.6733333,14.8795833 L17.74,14.9145833 C18.2804166,15.2204167 18.4904166,15.8879167 18.2066666,16.4404166 C17.9992177,16.8374371 17.5875294,17.0854789 17.1395833,17.0833471 L1.19375,17.0833471 C0.53458332,17.0833471 0,16.5625 0,15.92 C0,15.4795833 0.25541666,15.0766666 0.659999999,14.8795833 L0.736666659,14.8395833 C1.18716027,14.5903056 1.46767258,14.116941 1.47,13.6020833 L1.47,8.74999999 C1.47,4.97041666 4.5075,1.88208334 8.33374999,1.6775 C8.33332086,1.6739059 8.33318152,1.6702831 8.33333331,1.66666666 L8.33333331,0.833333339 C8.33333331,0.373096044 8.70642936,0 9.16666665,0 Z M9.59416665,3.33333334 L8.73874999,3.33333334 C5.66916665,3.33333334 3.18041666,5.75833333 3.18041666,8.74999999 L3.18041666,13.6025 C3.18041666,14.2275 2.98708332,14.8241666 2.64333332,15.3241666 L2.57666666,15.4166666 L15.7558333,15.4166666 C15.3888641,14.9244196 15.1794107,14.3326629 15.155,13.7191666 L15.1529166,13.6025 L15.1529166,8.74999999 C15.1529166,5.75833333 12.6641666,3.33333334 9.59416665,3.33333334 L9.59416665,3.33333334 Z"
            id="形状"></path>
    </svg>
);

const HeaderMenu = () => {
    const nav = useNavigate();
    const OnLogin = useCallback(() => {
        nav('/login');
    }, []);

    const OnContribute = useCallback(() => {
        nav('/articleEdit');
    }, []);
    return (
        <div className={styles['menu']}>
            <div className="flex align-center">
                <ul>
                    <li>
                        <NavLink to="/">产品</NavLink>
                    </li>
                    <li>
                        <NavLink to="/login">案例</NavLink>
                    </li>
                    <li>
                        <NavLink to="/article">服务</NavLink>
                    </li>
                    <li>
                        <NavLink to="/articleEdit">商城</NavLink>
                    </li>
                    <li>
                        <NavLink to="/articleInfo">售后</NavLink>
                    </li>
                    <li>
                        <NavLink to="/login">关于我们</NavLink>
                    </li>
                </ul>
                <Button type="primary" onClick={() => OnContribute()} className={styles['submit']}>
                    投稿
                </Button>
            </div>

            <div className="flex align-center">
                <div className={styles['vip']}>个人/企业VIP</div>
                <div className="ml-1 pointer">
                    <Icon component={SearshSvg} />
                </div>
                <div className="ml-1 pointer">
                    <Icon component={MessageSvg} />
                </div>
                <Button type="primary" className={styles['login-btn']} onClick={() => OnLogin()}>
                    登陆
                </Button>
                <Button type="primary" className={styles['reg-btn']}>
                    注册
                </Button>
            </div>
        </div>
    );
};

export default HeaderMenu;
