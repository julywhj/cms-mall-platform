import { listArticle } from '@/service/article/article';
import styles from '@/styles/article/article-list.m.less';
import { Image, Pagination } from 'antd';
import { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
const ArticleList = () => {
    const SIZE = 10;
    const [articleList, setArticleList] = useState<Article[]>([]);
    const [page, setPage] = useState(1);
    const [total, setTotal] = useState<number>(SIZE);
    const navigate = useNavigate();
    const getArticleList = async () => {
        const pageData = await listArticle({ page: page, size: SIZE });
        setArticleList(pageData.list);
        setTotal(pageData.total);
    };

    const toInfo = useCallback((id: number) => {
        navigate('/articleInfo/' + id);
    }, []);

    useEffect(() => {
        getArticleList();
    }, []);
    return (
        <div>
            {articleList.map((item, index) => {
                return (
                    <div key={index} className={styles['article-list']}>
                        <div className={styles['image']}>
                            <Image className={styles['image']} src={item.coverImg}></Image>
                        </div>
                        <div className={styles['conetent']} onClick={() => toInfo(item.id ? item.id : 0)}>
                            <div className={styles['title']}>{item.title}</div>
                            <div className={styles['avatar']}>
                                <div className={styles['img']}>
                                    <Image className={styles['img']} src="https://store.sumubook.com/sumo/images/Gnarf.png"></Image>
                                </div>
                                <div className={styles['name']}>{item.author ? item.author : '匿名'}</div>
                                <div className={styles['dian']}>-</div>
                                <div className={styles['create-time']}>{item.createTime}</div>
                            </div>
                            <div className={styles['brief']}>{item.articleIntroductory}</div>
                            <div className={styles['operation']}>
                                <div className="flex">
                                    <div className={styles['read-num']}>{item.readNum ? item.readNum : 0}</div>
                                    <div className={styles['comment-num']}>{item.commentNum ? item.commentNum : 0}</div>
                                    <div className={styles['like-num']}>{item.likeNum ? item.likeNum : 0}</div>
                                </div>
                                <div>
                                    <div className={styles['coffee-bean-number']}>已获得{item.coffeeBeanNumber ? item.coffeeBeanNumber : 0}咖啡豆 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            })}
            <div className={styles['pagination']}>
                <div>
                    <Pagination size="small" defaultCurrent={page} total={total} />
                </div>
            </div>
        </div>
    );
};
export default ArticleList;
