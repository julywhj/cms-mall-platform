import styles from '@/styles/article/article-page.m.less';
import ArticleSearch from './ArticleSearch';
import ArticleList from './ArticleList';
const ArticlePage = () => {
    return (
        <div className={styles['article-page']}>
            <div className="flex justify-center pt-2">
                <ArticleSearch />
            </div>
            <div>
                <ArticleList />
            </div>
        </div>
    );
};
export default ArticlePage;
