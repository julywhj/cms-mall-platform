import styles from '@/styles/article/article-search.m.less';
const ArticleSearch = () => {
    return (
        <div>
            <div className={styles['article-search-line']}></div>
            <div>搜索区域</div>
            <div className={styles['article-search-line']}></div>
            <div className={styles['article-search-tabs']}>
                <div className="flex">
                    <div className={styles['article-search-title']}>综合排序</div>
                    <div className={styles['article-search-lable']}>最新发布</div>
                    <div className={styles['article-search-lable']}>浏览最高</div>
                    <div className={styles['article-search-lable']}>热度最多</div>
                    <div className={styles['article-search-lable']}>最多下载</div>
                </div>
                <div className={styles['article-search-lable']}>1000个结果</div>
            </div>
        </div>
    );
};

export default ArticleSearch;
