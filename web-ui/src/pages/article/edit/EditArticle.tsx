import MyEditor from '@/components/MyEditor';
import { GetTags, SaveTags, articleClassifyTree } from '@/service';
import { saveArticle } from '@/service/article/article';
import { upLoadFile } from '@/service/file/file';
import styles from '@/styles/article/article-edit.m.less';
import { mergeClasses } from '@/utils';
import { DownOutlined, LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Cascader, Divider, Form, FormProps, GetProp, Input, Select, Upload, UploadProps, message } from 'antd';
import React, { useCallback, useEffect } from 'react';
import { useState } from 'react';
const { Option } = Select;
const EditArticle = () => {
    type FileType = Parameters<GetProp<UploadProps, 'beforeUpload'>>[0];
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState<string>();

    const [selectList, setSelectList] = useState<TagsType[]>([]);
    const [tags, setTags] = useState<string[]>([]);
    const [articleClassifyOption, setArticleClassifyOption] = useState<Option[]>([]);
    const [selectClassify, setSelectClassify] = useState<string[]>([]);
    const [htmlContent, setHtmlContent] = useState<string>('');

    const init = async () => {
        const tags = await GetTags('');
        setSelectList(tags);
        const classifyTree = await articleClassifyTree();
        setArticleClassifyOption(classifyTree);
    };

    useEffect(() => {
        init();
    }, []);

    const onChange = useCallback((value: any) => {
        setSelectClassify(value);
    }, []);

    const dropdownRender = (menus: React.ReactNode) => (
        <div>
            {menus}
            <Divider style={{ margin: 0 }} />
            <div style={{ padding: 8 }}>分类有问题请留言，小咖回及时修改哦</div>
        </div>
    );

    const onFinish: FormProps['onFinish'] = (values) => {
        console.log('Success:', values, selectClassify, htmlContent, tags);
        const article: Article = {
            title: values.title,
            articleIntroductory: values.articleIntroductory,
            articleContent: htmlContent,
            classifyIds: selectClassify.join(','),
            articleTags: tags.join(','),
            coverImg: imageUrl,
        };
        // 保存文章
        saveArticle(article).then((res) => {
            console.log(res);
        });
    };

    const onFinishFailed: FormProps['onFinishFailed'] = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const uploadButton = (
        <button style={{ border: 0, background: 'none' }} type="button">
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8, color: '#999' }}>上传封面</div>
        </button>
    );

    const customRequest = async (options: any) => {
        const { file, onSuccess } = options;
        const data = await upLoadFile(file);
        onSuccess(data.url);
        setImageUrl(data.url);
    };

    const beforeUpload = (file: FileType) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    };

    const MAX_COUNT = 3;

    const suffix = (
        <>
            <span>
                {tags.length} / {MAX_COUNT}
            </span>
            <DownOutlined />
        </>
    );
    const onKeyDown = useCallback(
        async (e: any) => {
            if (e.key === 'Enter') {
                // 这里可以添加你的逻辑
                if (e.target.value) {
                    const tag = await SaveTags({ tagName: e.target.value });
                    if (tag.id && tags.includes(tag.tagName)) {
                        return;
                    }
                    if (tag.id) {
                        setTags([...tags, tag.id]);
                        setSelectList([...selectList, { id: tag.id, tagName: tag.tagName }]);
                    }
                }
            }
        },
        [tags]
    );

    return (
        <div className={styles['edit']}>
            <Form name="basic" labelCol={{ span: 4 }} wrapperCol={{ span: 16 }} initialValues={{ remember: true }} onFinish={onFinish} onFinishFailed={onFinishFailed} autoComplete="off">
                <div className={styles['article-title']}>
                    <p>文章信息</p>
                    <div className={styles['line']}></div>
                    <div className="mt-3">
                        <Form.Item label="文章标题" name="title" rules={[{ required: true, message: 'Please input your username!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="文章简介" name="articleIntroductory" rules={[{ required: true, message: 'Please input your password!' }]}>
                            <Input.TextArea />
                        </Form.Item>
                    </div>
                </div>
                <div className={styles['article-content']}>
                    <p>文章内容</p>
                    <div className={styles['line']}></div>
                    <div className="m-1">
                        <MyEditor html={htmlContent} setHtml={setHtmlContent} />
                    </div>
                </div>
                <div className={styles['label-page']}>
                    <p>标签</p>
                    <div className={styles['line']}></div>
                    <div className="mt-3">
                        <Form.Item label="文章分类">
                            <Cascader onChange={onChange} options={articleClassifyOption} dropdownRender={dropdownRender} placeholder="请选择分类" />
                        </Form.Item>
                        <Form.Item label="标签">
                            <Select mode="multiple" maxCount={MAX_COUNT} value={tags} style={{ width: '100%' }} onChange={setTags} onKeyDown={onKeyDown} suffixIcon={suffix} placeholder="请选择标签" showSearch>
                                {selectList && selectList.length > 0
                                    ? selectList.map((item, index) => {
                                          return (
                                              <Option value={item.tagName} key={item.id}>
                                                  {item.tagName}
                                              </Option>
                                          );
                                      })
                                    : null}
                            </Select>
                        </Form.Item>
                    </div>
                </div>
                <div className={styles['cover']}>
                    <p>上传封面</p>
                    <div className={styles['line']}></div>
                    <div className="mt-3 ml-3">
                        <Upload name="avatar" listType="picture-card" showUploadList={false} customRequest={customRequest} beforeUpload={beforeUpload}>
                            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                        </Upload>
                    </div>
                </div>
                <div className={styles['submit']}>
                    <div>
                        <Button htmlType="submit" className={styles['submit-btn']}>
                            发 布
                        </Button>
                        <Button className={mergeClasses(styles['save-btn'], styles['submit-btn'])}>保 存</Button>
                    </div>
                    <div className={styles['drafts']}>草稿箱</div>
                </div>
            </Form>
        </div>
    );
};
export default EditArticle;
