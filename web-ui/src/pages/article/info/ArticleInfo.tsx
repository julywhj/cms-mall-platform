import styles from '@/styles/article/article-info.m.less';
import Advertising from './Advertising';
import { Image } from 'antd';
import HtmlContent from '@/components/HtmlContent';
import Comment from '@/components/Comment';
import RecommendedArticles from './RecommendedArticles';
import { useEffect, useState } from 'react';
import { infoArticle } from '@/service/article/article';
import { useParams } from 'react-router-dom';
const ArticleInfo = () => {
    const params = useParams();
    const [article, setArticle] = useState<Article>();
    const getArticleInfo = async () => {
        const article = await infoArticle(Number(params.id));
        setArticle(article);
    };
    useEffect(() => {
        getArticleInfo();
    }, []);

    const commentList = [
        { userName: 'whj', commentContent: '塞俄比亚古国绿树枝头上的野生红', articleId: 0, userId: 0, createTime: '2024-04-02', avatar: 'https://store.sumubook.com/sumo/images/Gnarf.png' },
        { userName: 'aa', commentContent: '为当今世界上三大饮料之一', articleId: 0, userId: 0, createTime: '2024-04-02', avatar: 'https://store.sumubook.com/sumo/images/Gnarf.png' },
    ];

    return (
        <div className={styles['info']}>
            <div>
                {/* 广告 */}
                <Advertising />
                <div className={styles['line']}></div>
                {/* 文章展示区域 */}
                <div className={styles['article-content']}>
                    {/* 文章内容区域 */}
                    <div className={styles['content']}>
                        <div className={styles['title']}>{article?.title}</div>
                        <div className="flex justify-between align-center mt-2">
                            <div className={styles['avatar']}>
                                <div className={styles['img']}>
                                    <Image className={styles['img']} src="https://store.sumubook.com/sumo/images/Gnarf.png"></Image>
                                </div>
                                <div className={styles['name']}>{article?.author}</div>
                                <div className={styles['dian']}>-</div>
                                <div className={styles['create-time']}>{article?.createTime}</div>
                            </div>
                            <div className={styles['follow-btn']}>关注</div>
                        </div>
                        <div className="mt-2">
                            <HtmlContent html={article?.articleContent ? article?.articleContent : ''} />
                        </div>
                        <div className="flex justify-center align-center mt-2">
                            <div className={styles['like-btn']}>{article?.likeNum}</div>
                        </div>
                        {/* 评论 */}
                        <div>
                            <Comment {...{ articleId: 1, commentList: commentList }} />
                        </div>
                        <div className={styles['line-comment']}></div>
                        {/* 相关推荐 */}
                        <div>
                            <RecommendedArticles />
                        </div>
                        <div className={styles['no-data']}>没有更多内容啦～～</div>
                    </div>
                    {/* 数据和相关推荐区域 */}
                    <div>
                        <div className={styles['article-num']}>
                            <ul>
                                <li>点赞数：{article?.likeNum}</li>
                                <li>浏览量：{article?.readNum}</li>
                                <li>分类：{article?.classifyIds}</li>
                                <li>标签：{article?.articleTags}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default ArticleInfo;
