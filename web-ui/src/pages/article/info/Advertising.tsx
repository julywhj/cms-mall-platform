import styles from '@/styles/article/advertising.m.less';
import { Image } from 'antd';
const Advertising = () => {
    return (
        <div className={styles['advertising']}>
            <Image preview={false} src="https://file.sumubook.com/20240420/af604e0792ac4627b174de0a974a201b.png"></Image>
        </div>
    );
};
export default Advertising;
