import { listArticle } from '@/service/article/article';
import styles from '@/styles/article/recommended-articles.m.less';
import { useEffect, useState } from 'react';
import { Image } from 'antd';
const RecommendedArticles = () => {
    const SIZE = 4;
    const [articleList, setArticleList] = useState<Article[]>([]);
    const getArticleList = async () => {
        const pageData = await listArticle({ pageNum: 1, pageSize: SIZE });
        setArticleList(pageData.list);
    };
    useEffect(() => {
        getArticleList();
    }, []);
    return (
        <div className={styles['recommended-articles']}>
            <div className={styles['title']}>更多内容推荐</div>
            <div>
                {articleList.map((item, index) => {
                    return (
                        <div key={index} className={styles['article-list']}>
                            <div className={styles['image']}>
                                <Image className={styles['image']} src={item.coverImg}></Image>
                            </div>
                            <div className={styles['conetent']}>
                                <div className={styles['title']}>{item.title}</div>
                                <div className={styles['brief']}>{item.articleIntroductory}</div>
                                <div className={styles['operation']}></div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default RecommendedArticles;
