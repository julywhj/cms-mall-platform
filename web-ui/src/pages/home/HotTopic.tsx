import styles from '@/styles/home/hot-topic.m.less';
const HotTopic = () => {
    return (
        <div className={styles['hot-topic']}>
            <div className={styles['bg']}></div>
            <div className={styles['hot-title']}>趋势热点</div>
            <div>
                <ul>
                    <li>
                        <div className={styles['first-title']}>01</div>
                        <div className={styles['content']}>如何查看智能总结？</div>
                        <div className={styles['hot-icon']}>热</div>
                    </li>
                    <li>
                        <div className={styles['first-title']}>02</div>
                        <div className={styles['content']}>如何查看智能总结？</div>
                        <div className={styles['new-icon']}>新</div>
                    </li>
                    <li>
                        <div className={styles['other-title']}>03</div>
                        <div className={styles['content']}>如何查看智能总结？</div>
                    </li>
                </ul>
            </div>
        </div>
    );
};
export default HotTopic;
