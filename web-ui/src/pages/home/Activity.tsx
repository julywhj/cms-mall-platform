import styles from '@/styles/home/activity.m.less';
import { Carousel, Image } from 'antd';
const Activity = () => {
    const onChange = (currentSlide: number) => {
        console.log(currentSlide);
    };
    return (
        <div className={styles['activity']}>
            <Carousel afterChange={onChange} autoplay={true}>
                <div>
                    <Image className={styles['img']} width={765} height={400} src="https://store.sumubook.com/sumo/images/woman-4246954_1280.jpg" />
                </div>
                <div>
                    <Image className={styles['img']} width={765} height={400} src="https://store.sumubook.com/sumo/images/coffee-1869647_1280.jpg" />
                </div>
                <div>
                    <Image className={styles['img']} width={765} height={400} src="https://store.sumubook.com/sumo/images/coffee-4334647_1280.jpg" />
                </div>
                <div>
                    <Image className={styles['img']} width={765} height={400} src="https://store.sumubook.com/sumo/images/coffee-1974841_1280.jpg" />
                </div>
            </Carousel>
        </div>
    );
};

export default Activity;
