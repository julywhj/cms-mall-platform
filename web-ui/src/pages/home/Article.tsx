import styles from '@/styles/home/article.m.less';
import { Image } from 'antd';
const Article = () => {
    return (
        <div className={styles['article']}>
            <div className={styles['item']}>
                <div className={styles['image']}>
                    <Image className={styles['image']} src="https://store.sumubook.com/sumo/images/kafeiji.jpg"></Image>
                </div>
                <div className={styles['content']}>
                    <div className={styles['title']}>带秤研磨一体咖啡机</div>
                    <div>
                        <ul>
                            <li>内置多功能电子秤</li>
                            <li>20Bar高压萃取</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className={styles['item']}>
                <div className={styles['image']}>
                    <Image className={styles['image']} src="https://img12.360buyimg.com/n1/jfs/t1/166292/18/42056/113610/65ee77a3Fc20bd284/246b17b575abbd55.jpg"></Image>
                </div>
                <div className={styles['content']}>
                    <div className={styles['title']}>博达轻奢蓝山咖啡豆 纯黑咖啡进口生豆新鲜烘焙</div>
                    <div>
                        <ul>
                            <li>花果香</li>
                            <li>烤杏仁</li>
                            <li>鲜奶油</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className={styles['item']}>
                <div className={styles['image']}>
                    <Image className={styles['image']} src="https://store.sumubook.com/sumo/images/kafeiji.jpg"></Image>
                </div>
                <div className={styles['content']}>
                    <div className={styles['title']}>带秤研磨一体咖啡机</div>
                    <div>
                        <ul>
                            <li>内置多功能电子秤</li>
                            <li>20Bar高压萃取</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className={styles['change']}>换一批</div>
        </div>
    );
};

export default Article;
