import Article from './Article';
import styles from '@/styles/home/story-area.m.less';
const StoryArea = () => {
    return (
        <div className={styles['story-area']}>
            <div className="flex justify-between align-center">
                <p>咖啡故事</p>
                <div className={styles['more']}>查看更多</div>
            </div>
            <Article />
        </div>
    );
};
export default StoryArea;
