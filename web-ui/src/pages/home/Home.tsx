import Topic from '@/components/Topic';
import styles from '@/styles/home.m.less';
import PrimaryArea from './PrimaryArea';
import HotTopic from './HotTopic';
import ShopRecommendedArea from './ShopRecommendedArea';
import Classify from './Classify';
import Activity from './Activity';
import StoryArea from './StoryArea';
const Home = () => {
    return (
        <div className={styles['content']}>
            <Topic />
            <div className={styles['home-first-page']}>
                <div className="flex justify-center">
                    <PrimaryArea />
                    <HotTopic />
                    <ShopRecommendedArea />
                </div>
            </div>
            <div className={styles['home-classify-page']}>
                <Classify />
                <Activity />
                {/* 推荐文章 */}
                <StoryArea />
            </div>
            <div className="flex justify-center">
                <div className={styles['knowledge']}>
                    <div>
                        <p>一、咖啡文化</p>
                        <p>二、咖啡食谱</p>
                        <p>三、咖啡故事</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
