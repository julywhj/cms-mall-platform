import styles from '@/styles/home/shop-recommend-area.m.less';
import Product from './Product';
const ShopRecommendedArea = () => {
    return (
        <div className={styles['shop']}>
            <p>推荐产品</p>
            <Product />
        </div>
    );
};

export default ShopRecommendedArea;
