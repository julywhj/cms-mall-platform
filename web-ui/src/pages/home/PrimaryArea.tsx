import styles from '@/styles/home/primary-area.m.less';
import { mergeClasses } from '@/utils';
import { Carousel, Image } from 'antd';
const PrimaryArea = () => {
    const onChange = (currentSlide: number) => {
        console.log(currentSlide);
    };
    return (
        <div className={styles['primary-area']}>
            <Carousel afterChange={onChange}>
                <div>
                    <Image className={styles['img']} width={593} height={300} src="https://store.sumubook.com/sumo/images/coffee-1276778_640.jpg" />
                </div>
            </Carousel>
            <div className={styles['titles']}>
                <div>
                    <div className="flex align-end justify-between">
                        <div className={styles['title']}>竹代塑对传统塑料包装企业的影响</div>
                        <div className={styles['time']}>2023-06-08</div>
                    </div>
                    <div className="flex mt-1">
                        <div className={styles['author']}>作者：王洪杰</div>
                        <div className={mergeClasses(styles['author'], 'ml-4')}>策划：张先生</div>
                    </div>
                    <div className={mergeClasses(styles['tabs'], 'mt-1')}>
                        <div>#咖啡</div>
                        <div className="ml-2">#烘焙</div>
                        <div className="ml-2">#读书</div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default PrimaryArea;
