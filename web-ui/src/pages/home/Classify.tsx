import styles from '@/styles/home/classify.m.less';

const Classify = () => {
    return (
        <div className={styles['classify']}>
            <div>
                <p>咖啡机</p>
                <p>咖啡豆</p>
                <p>手冲咖啡</p>
                <p>咖啡知识</p>
                <p>日常分享</p>
            </div>
        </div>
    );
};
export default Classify;
