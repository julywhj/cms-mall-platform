import styles from '@/styles/components/login/register.m.less';
import { Button } from 'antd';
const RegisterSuccess = () => {
    const ToLogin = () => {
        console.log('');
    };
    return (
        <div className={styles['register']}>
            <div className={styles['register-icon']}></div>
            <div className={styles['register-title']}>
                您已成功注册！
                <p>欢迎加入芭蕉网</p>
            </div>
            <div>
                <Button onClick={() => ToLogin()} className={styles['register-btn']} type="primary">
                    去登陆
                </Button>
            </div>
        </div>
    );
};
export default RegisterSuccess;
