import VerificationCode from '@/components/VerificationCode';
import styles from '@/styles/components/login/retrievePassword.m.less';
import { Button, Input } from 'antd';
import { useCallback, useState } from 'react';
const RetrievePassword = () => {
    //手机号
    const [account, setAccount] = useState<string>('');
    const [code, setCode] = useState<string>('');
    const [uid, setUid] = useState<string>('');
    const [next, setNext] = useState<boolean>(false);

    const OnNext = useCallback(() => {
        console.log(uid, code);
        setNext(true);
    }, []);

    const PhoneVerify = (
        <div>
            <Input className={styles['account-input']} placeholder="* 请输入手机号" value={account} onChange={(e) => setAccount(e.target.value)} />
            <VerificationCode phone={account} setCode={setCode} setUid={setUid} className={styles['account-input']} />
        </div>
    );

    const ChangePassword = (
        <div>
            <Input className={styles['account-input']} placeholder="* 请输入新密码" value={account} onChange={(e) => setAccount(e.target.value)} />

            <Input className={styles['account-input']} placeholder="* 请再次输入密码" value={account} onChange={(e) => setAccount(e.target.value)} />
        </div>
    );

    return (
        <div className={styles['retrieve-password']}>
            <div className={styles['register-title']}>找回密码</div>
            {next ? ChangePassword : PhoneVerify}
            <div>
                <Button
                    onClick={() => {
                        OnNext();
                    }}
                    className={styles['login-btn']}
                    type="primary">
                    {next ? '提交' : '下一步'}
                </Button>
            </div>
        </div>
    );
};

export default RetrievePassword;
