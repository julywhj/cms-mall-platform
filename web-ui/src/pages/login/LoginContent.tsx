import styles from '@/styles/components/login/login.m.less';
import WeChatLogin from './WeChatLogin';
import { useState } from 'react';
import PhoneLogin from './PhoneLogin';
import RetrievePassword from './RetrievePassword';
import Register from './Register';
const LoginContent = () => {
    const [loginType, setLoginType] = useState<string>('phoneLogin');

    const ShowContent = (loginType: string) => {
        if (loginType === 'weChatLogin') {
            return <WeChatLogin setLoginType={setLoginType} />;
        } else if (loginType === 'phoneLogin') {
            return <PhoneLogin setLoginType={setLoginType} />;
        } else if (loginType === 'register') {
            return <Register setLoginType={setLoginType} />;
        } else if (loginType === 'retrievePassword') {
            return <RetrievePassword />;
        }
    };
    return (
        <div className={styles['login']}>
            <div className={styles['title']}>技术服务方案引领者</div>
            <div className={styles['content']}>
                <div className="flex align-center">
                    <div className={styles['icon']}></div>
                    <div className="ml-3">
                        <div>海量服务方案库持续更新中</div>
                        <div className={styles['label']}>电商场景全覆盖</div>
                    </div>
                </div>
                <div className="flex align-center mt-3">
                    <div className={styles['team']}></div>
                    <div className="ml-3">
                        <div>与技术大咖一起学习实践</div>
                        <div className={styles['label']}>技术创新无处不在，引领产业发展新未来</div>
                    </div>
                </div>
                <div className="flex align-center mt-3">
                    <div className={styles['service']}></div>
                    <div className="ml-3">
                        <div>提供专业方案咨询服务</div>
                        <div className={styles['label']}>助力制造业降本增效，提升品牌形象</div>
                    </div>
                </div>
            </div>
            {ShowContent(loginType)}
        </div>
    );
};
export default LoginContent;
