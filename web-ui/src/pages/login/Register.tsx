import styles from '@/styles/components/login/register.m.less';
import { Button, Checkbox, Input } from 'antd';
import { useCallback, useState } from 'react';
import HttpUtils from '@utils/HttpUtils';
import { handelPromise } from '@/utils';
import VerificationCode from '@components/VerificationCode';

//账号
const Register = (props: LoginProps) => {
    //账号
    const [password, setPassword] = useState<string>('');
    //手机号
    const [phone, setPhone] = useState<string>('');
    const [code, setCode] = useState<string>('');
    const [uid, setUid] = useState<string>('');
    const [agreementDisabled, setAgreementDisabled] = useState<boolean>(false);

    const OnRegister = useCallback(() => {
        handelPromise(
            HttpUtils.Post('/api/v2/register/phoneRegister', {
                username: phone,
                nickname: phone,
                mobile: phone,
                password: password,
                roleIds: [0],
            })
        );
    }, [phone, password, code, uid]);

    return (
        <div className={styles['register']}>
            <div onClick={() => props.setLoginType('register')} className={styles['register-title']}>
                注册账号
            </div>
            <div>
                <Input className={styles['account-input']} placeholder="* 请输入手机号/账号" value={phone} onChange={(e) => setPhone(e.target.value)} />
                <Input className={styles['account-input']} placeholder="*6 - 16 位密码，区分大小写" value={password} onChange={(e) => setPassword(e.target.value)} />
                <Input className={styles['account-input']} placeholder="*请再次确认密码" value={password} onChange={(e) => setPassword(e.target.value)} />
                <VerificationCode {...{ setCode, setUid, phone, className: styles['account-input'] }} />
            </div>
            <div>
                <Button onClick={() => OnRegister()} className={styles['login-btn']} type="primary">
                    注册
                </Button>
            </div>
            <div className={styles['agreement']}>
                <Checkbox checked={agreementDisabled} onChange={(e) => setAgreementDisabled(e.target.checked)} />
                &nbsp; 我已阅读并同意<span>用户服务协议、</span>
                <span>隐私政策、</span>
                <span>知识产权声明</span>
            </div>
        </div>
    );
};

export default Register;
