import styles from '@/styles/components/login/phone.m.less';
import { useCallback, useState } from 'react';
import { mergeClasses } from '@utils/index';
import { Button, Checkbox, Input } from 'antd';
import { PhoneLoginApi, SmsCodeLogin } from '@/service';
import VerificationCode from '@components/VerificationCode';

const PhoneLogin = (props: LoginProps) => {
    const [passwordType, setPasswordType] = useState<boolean>(true);
    const [agreementDisabled, setAgreementDisabled] = useState<boolean>(false);
    //账号
    const [account, setAccount] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    //手机号
    const [phone, setPhone] = useState<string>('');
    const [code, setCode] = useState<string>('');
    const [uid, setUid] = useState<string>('');

    const Login = useCallback(async () => {
        console.log(account, password, phone, code, uid);
        if (passwordType) {
            PhoneLoginApi(account, password);
        } else {
            SmsCodeLogin(phone, code, uid);
        }
    }, [account, password, passwordType, phone, code, uid]);

    //账号密码
    const PasswordContent = (
        <div>
            <Input className={styles['account-input']} placeholder="请输入手机号/账号" value={account} onChange={(e) => setAccount(e.target.value)} />

            <Input.Password className={styles['account-input']} placeholder="请输入密码" value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>
    );
    // 短信验证
    const CodeContent = (
        <div>
            <Input value={phone} className={styles['account-input']} placeholder="请输入手机号" onChange={(e) => setPhone(e.target.value)} />
            <VerificationCode {...{ setCode, setUid, phone, className: styles['account-input'] }} />
        </div>
    );
    return (
        <div className={styles['phone']}>
            <div className={styles['login-method']}>
                <div onClick={() => setPasswordType(true)} className={passwordType ? mergeClasses(styles['login-title'], styles['active']) : styles['login-title']}>
                    密码登陆
                </div>
                <div onClick={() => setPasswordType(false)} className={passwordType ? styles['login-title'] : mergeClasses(styles['login-title'], styles['active'])}>
                    短信登陆
                </div>
            </div>

            {passwordType ? PasswordContent : CodeContent}
            <div className={styles['register']}>
                <span onClick={() => props.setLoginType('retrievePassword')}>忘记密码</span>
                <span onClick={() => props.setLoginType('register')}>立即注册</span>
            </div>
            <div>
                <Button onClick={() => Login()} className={styles['login-btn']} type="primary">
                    登录
                </Button>
            </div>

            <div className={styles['agreement']}>
                <Checkbox checked={agreementDisabled} onChange={(e) => setAgreementDisabled(e.target.checked)} />
                &nbsp; 我已阅读并同意<span>用户服务协议、</span>
                <span>隐私政策、</span>
                <span>知识产权声明</span>
            </div>

            <div className={styles['bubble-tip']}>微信扫码登录</div>
            <div className={styles['right-top-triangle']}>
                <div className={styles['icon-weixin']}></div>
            </div>
            <div onClick={() => props.setLoginType('weChatLogin')} className={styles['left-bottom-triangle']}></div>
        </div>
    );
};

export default PhoneLogin;
