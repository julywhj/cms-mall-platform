import styles from '@/styles/components/login/wechat.m.less';
import { Checkbox } from 'antd';
import { useState } from 'react';

const WeChatLogin = (props: LoginProps) => {
    const [agreementDisabled, setAgreementDisabled] = useState<boolean>(false);
    return (
        <div className={styles['wechat']}>
            <div className={styles['login-title']}>微信扫码一键登录</div>
            <div className={styles['login-lable']}>未注册的微信号将自动创建账号</div>
            <div className={styles['qr-code']}></div>
            <div className={styles['agreement']}>
                <Checkbox checked={agreementDisabled} onChange={(e) => setAgreementDisabled(e.target.checked)} />
                &nbsp; 我已阅读并同意<span>用户服务协议、</span>
                <span>隐私政策、</span>
                <span>知识产权声明</span>
            </div>
            <div className={styles['register']}>
                没有账号？<span>立即注册</span>
            </div>

            <div className={styles['bubble-tip']}>手机号登录</div>
            <div className={styles['right-top-triangle']}>
                <div className={styles['icon-phone']}></div>
            </div>
            <div onClick={() => props.setLoginType('phoneLogin')} className={styles['left-bottom-triangle']}></div>
        </div>
    );
};
export default WeChatLogin;
