import _ from 'lodash';
import { message } from 'antd';
export * from './localstorage';
/**
 * 合并class
 * @param classeNames
 * @returns
 */
export const mergeClasses = (...classeNames: string[]): string => {
    const _classNames: string[] = [];
    classeNames.filter((o) => !_.isEmpty(o)).forEach((o) => _classNames.push(o));
    return _classNames.join(' ');
};

export const handelPromise = <T>(fn: Promise<T>, loadingFn?: any): Promise<T> => {
    if (loadingFn !== undefined) {
        loadingFn(true);
    }
    return new Promise((res) => {
        fn.then((tran) => {
            res(tran);
        })
            .catch((e) => {
                console.error(e);
                message.error(e.data.msg);
            })
            .finally(() => {
                if (loadingFn !== undefined) {
                    loadingFn(false);
                }
            });
    });
};
