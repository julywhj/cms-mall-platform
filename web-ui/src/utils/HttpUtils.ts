import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import QueryString from 'qs';
declare type HttpConfig = { Authorization?: string; Timeout?: number; CurrentTime?: number; Address?: string; WalletType?: string; Chain?: string; Language?: string; DAppKey?: string; ContentType?: string; Accept?: string };

declare type ResponseResult = { code: string; data: any; msg: string };

class HttpUtils {
    private UriPrefix = `${process.env.REACT_APP_API_HOST}`;
    private readonly request: AxiosInstance;
    public constructor() {
        this.request = axios.create();
        this.request.defaults.headers['Content-Type'] = 'application/json;charset=UTF-8;';
        this.request.defaults.headers['Accept'] = 'application/json';
        this.request.interceptors.response.use(
            (o) => this.ParseResponseBodySuccess(o),
            (o) => this.ParseResponseBodyError(o)
        );
        this.request.defaults.baseURL = this.UriPrefix;
    }
    private Config: HttpConfig = {};

    private ParseResponseBodySuccess(response: AxiosResponse) {
        const data = response.data as ResponseResult;
        if (!data.code) {
            return Promise.reject(`data error ${data}`);
        }
        if (data.code != '00000') {
            return Promise.reject(`data error ${data.msg}`);
        }
        return Promise.resolve(data.data);
    }

    private ParseResponseBodyError(error: AxiosError) {
        return Promise.reject(error.response);
    }

    public UnLogin() {
        delete this.Config.Authorization;
    }

    public Login(config: HttpConfig) {
        this.Config = { ...this.Config, ...(config || {}) };
        if (this.Config.Authorization) {
            this.request.defaults.headers['Authorization'] = this.Config.Authorization;
        }
    }
    public SetUriPrefix(uriPrefix: string) {
        this.UriPrefix = uriPrefix;
        this.request.defaults.baseURL = this.UriPrefix;
    }

    private MergeConfig(c: HttpConfig): AxiosRequestConfig {
        const config = { headers: {} } as AxiosRequestConfig;
        if (!c) {
            return config;
        }
        if (c.ContentType) {
            if (config && config.headers) {
                config.headers['Content-Type'] = c.ContentType;
            }
        }
        if (c.Accept) {
            if (config && config.headers) {
                config.headers['Accept'] = c.Accept;
            }
        }
        if (c.Authorization) {
            if (config && config.headers) {
                config.headers['Authorization'] = c.Authorization;
            }
        }
        if (c.Timeout) {
            config.timeout = c.Timeout;
        }
        return config;
    }

    public Post(path: string, body: any, config: HttpConfig = {}) {
        return this.request.post(path, body, this.MergeConfig(config));
    }

    public UploadFile(path: string, body: any, config: HttpConfig = {}) {
        return this.request.post(path, body, this.MergeConfig(config));
    }

    public Get(path: string, params: { [key in string]?: any } = {}, config: HttpConfig = {}) {
        // eslint-disable-next-line no-param-reassign
        path = path + (path.includes('?') ? '&' : '?') + QueryString.stringify(params || {});
        // trim ?
        if (path.endsWith('?')) {
            // eslint-disable-next-line no-param-reassign
            path = path.substring(0, path.length - 1);
        }
        return this.request.get(path, this.MergeConfig(config));
    }
}

export default new HttpUtils();
